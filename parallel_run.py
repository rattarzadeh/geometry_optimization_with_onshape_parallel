
import isodate
import time
import argparse
import csv
from simscale_sdk import *
import onshape_client
import os
from logging import exception
import numpy as np
from datetime import datetime
#from utils.geometry import XtoStr
from simscale import SimScaleProject

from subprocess import Popen, PIPE
import uuid
import json


XtoStr = lambda X: "_".join([f"{xi:.1f}" for xi in X])

angular_velocity = 185.35397
P=[]

#Setting working directory
CAD_PATH = "/Users/rattarzadeh/Desktop/git/geometry-optimization-with-simscale-and-onshape-PARAM/geometry_optimization_with_onshape_parallel/CAD"

#Setting Simscale API config
API_KEY = os.getenv('SIMSCALE_API_KEY')
API_URL = os.getenv('SIMSCALE_API_URL')
PID = 8448552506094058731   #GeoOpti-from-Disk
default_project = SimScaleProject(API_KEY, API_URL, PID)  


#Setting Onshape API config
onshape_data = {
    "did": "6e6c2e0f8f96f4862cfbb1c4",
    "wid": "4ce0cf3c261f309c355cc56b",
    "eid": "11ad7d6621bfeb5a95e6b17f",
}

# Search space bounds
# For debbuging keeping only one point
#    h1,  h2,  h3,  h4, hp1, hp2,  v1,  v2,  v3,  v4,  r1,  r2
BBladeAngle = [50]#[20,30, 40, 45, 50]#, 50]#, 40, 50]  #20  ######
BBladeThickness = [1]#, 3, 5, 7]#[1, 3, 5, 7]#, 5]#, 7]    
BBladeLeadingEdgeR = [0.5]#[0.2, 0.5, 0.7]
BBladeCount = [10]#[8, 10, 12]
DDiffuserDraftAngle = [5]#[3, 4, 5, 6],
DDiffuserLength = [300]#, 400]#[250, 300, 400]
IInletPipeLength = [350]#, 400]
FFlowRate = [0.186]#, 0.1, 0.8]#[0.1, 0.186]#, 0.25]#, 0.55]#, 0.86]



def MultiGenCADOnShape(P, CAD_PATH, ONSHAPE_DATA):
    """
    Generate multiple CAD files from geometry parameters (*.x_t)

    All files are saved in the CAD_PATH folder

    Parameters
    ----------
    P: List of Lists with geometry parameters
    CAD_PATH: Path folder where CADs are stored

    Returns
    -------
    fileNameBases: List of full paths to the exported CAD files
    """
    fileNames = []

    # Onshape data (not necassiry because it has already been populated, but keep it for debugging)
    did = onshape_data["did"]
    wid = onshape_data["wid"]
    eid = onshape_data["eid"]
    
    os_client = onshape_client.Client()

    # ToDo
    # Make this loop parallel for speed.
    for X in P:
        if len(X) != 8:
            raise Exception(f"Invalid parameters vector: {X}")

        filename = CAD_PATH + XtoStr(X) + ".x_t"

        config = (
            f"h1={X[0]} mm;"
            f"h2={X[1]} mm;"
            f"h3={X[2]} mm;"
            f"h4={X[3]} mm;"
            f"hp1={X[4]} mm;"
            f"hp2={X[5]} mm;"
            f"v1={X[6]} mm;"
        )

        ps = os_client.part_studios_api.export_ps1(
            did, "w", wid, eid, configuration=config, _preload_content=False
        )

        with open(filename, "wb") as f:
            f.write(ps.data)

        fileNames.append(filename)

    return fileNames

def SimulationSpec(project=default_project):
    """
    Creates and returns the simulation spec model

    Parameters
    ----------
    project: SimScaleProject object where to create the geometry objects

    Returns
    -------
    model: Modeling of the pump
    """


    model = Incompressible(
        type="INCOMPRESSIBLE",
        turbulence_model="KOMEGASST",
        time_dependency=StationaryTimeDependency(
                type="STATIONARY",
        ),
        algorithm="SIMPLE",
        num_of_passive_species=0,
        model=FluidModel(),
        materials=IncompressibleFluidMaterials(
                fluids=[
                        IncompressibleMaterial(
                                type="INCOMPRESSIBLE",
                                name="Water",
                                viscosity_model=NewtonianViscosityModel(
                                        type="NEWTONIAN",
                                        kinematic_viscosity=DimensionalKinematicViscosity(
                                                value=9.3379E-7,
                                                unit="m²/s",
                                        ),
                                ),
                                density=DimensionalDensity(
                                        value=997.33,
                                        unit="kg/m³",
                                ),
                                topological_reference=TopologicalReference(
                                        entities=[
                                                "B2_TE1",
                                        ],
                                        sets=[],
                                ),
                                built_in_material="builtInWater",
                        ),
                ],
        ),
        initial_conditions=FluidInitialConditions(
                gauge_pressure=DimensionalInitialConditionDomainsPressure(
                        _global=DimensionalPressure(
                                value=0.0,
                                unit="Pa",
                        ),
                ),
                velocity=DimensionalVectorInitialConditionDomainsSpeed(
                        _global=DimensionalVectorSpeed(
                                value=DecimalVector(
                                        x=0.0,
                                        y=0.0,
                                        z=0.0,
                                ),
                                unit="m/s",
                        ),
                        subdomains=[],
                ),
                turbulent_kinetic_energy=DimensionalInitialConditionDomainsTurbulenceKineticEnergy(
                        _global=DimensionalTurbulenceKineticEnergy(
                                value=0.00375,
                                unit="m²/s²",
                        ),
                ),
                omega_dissipation_rate=DimensionalInitialConditionDomainsSpecificTurbulenceDissipationRate(
                        _global=DimensionalSpecificTurbulenceDissipationRate(
                                value=3.375,
                                unit="1/s",
                        ),
                ),
        ),
        boundary_conditions=[
                VelocityInletBC(
                        name="Velocity inlet 1",
                        type="VELOCITY_INLET_V3",
                        velocity=FlowRateInletVBC(
                                type="FLOW_RATE_INLET_VELOCITY",
                                flow_rate=VolumetricFlow(
                                        type="VOLUMETRIC",
                                        value=DimensionalFunctionVolumetricFlowRate(
                                                value=ConstantFunction(
                                                        type="CONSTANT",
                                                        value=0.186,
                                                ),
                                                unit="m³/s",
                                        ),
                                ),
                        ),
                        topological_reference=TopologicalReference(
                                entities=[
                                        "B2_TE3336",
                                ],
                                sets=[],
                        ),
                ),
                PressureOutletBC(
                        name="Pressure outlet 2",
                        type="PRESSURE_OUTLET_V30",
                        gauge_pressure=FixedValuePBC(
                                type="FIXED_VALUE",
                                value=DimensionalFunctionPressure(
                                        value=ConstantFunction(
                                                type="CONSTANT",
                                                value=0.0,
                                        ),
                                        unit="Pa",
                                ),
                        ),
                        topological_reference=TopologicalReference(
                                entities=[
                                        "B2_TE3830",
                                ],
                                sets=[],
                        ),
                ),
                WallBC(
                        name="Wall 3",
                        type="WALL_V34",
                        velocity=RotatingWallVBC(
                                type="ROTATING_WALL_VELOCITY",
                                rotation=AngularRotation(
                                        type="ANGULAR_ROTATION",
                                        rotation_center=DimensionalVectorLength(
                                                value=DecimalVector(
                                                        x=0.0,
                                                        y=0.0,
                                                        z=0.0,
                                                ),
                                                unit="m",
                                        ),
                                        rotation_axis=DimensionalVectorLength(
                                                value=DecimalVector(
                                                        x=-1.0,
                                                        y=0.0,
                                                        z=0.0,
                                                ),
                                                unit="m",
                                        ),
                                        angular_velocity=DimensionalFunctionRotationSpeed(
                                                value=ConstantFunction(
                                                        type="CONSTANT",
                                                        value=0.0,
                                                ),
                                                unit="rad/s",
                                        ),
                                ),
                                turbulence_wall="WALL_FUNCTION",
                        ),
                        topological_reference=TopologicalReference(
                                entities=[
                                        "B2_TE1331",
                                        "B2_TE1340",
                                        "B2_TE1345",
                                ],
                                sets=[],
                        ),
                ),
        ],
        advanced_concepts=AdvancedConcepts(
                rotating_zones=[
                        MRFRotatingZone(
                                type="MULTI_REFERENCE_FRAME",
                                name="MRF 1770 RPM",
                                origin=DimensionalVectorLength(
                                        value=DecimalVector(
                                                x=0.0,
                                                y=0.0,
                                                z=0.0,
                                        ),
                                        unit="m",
                                ),
                                axis=DimensionalVectorLength(
                                        value=DecimalVector(
                                                x=-1.0,
                                                y=0.0,
                                                z=0.0,
                                        ),
                                        unit="m",
                                ),
                                angular_velocity=DimensionalFunctionRotationSpeed(
                                        value=ConstantFunction(
                                                type="CONSTANT",
                                                value=185.35397,
                                        ),
                                        unit="rad/s",
                                ),
                                topological_reference=TopologicalReference(
                                        entities=[
                                                "B1_TE58",
                                        ],
                                        sets=[],
                                ),
                        ),
                ],
                porous_mediums=[],
                momentum_sources=[],
        ),
        numerics=FluidNumerics(
                relaxation_type="MANUAL",
                relaxation_factor=RelaxationFactor(
                        pressure_field=0.3,
                        velocity_equation=0.7,
                        turbulent_kinetic_energy_equation=0.7,
                        omega_dissipation_rate_equation=0.7,
                ),
                num_non_orthogonal_correctors=2,
                pressure_reference_cell=0,
                pressure_reference_value=DimensionalPressure(
                        value=0.0,
                        unit="Pa",
                ),
                residual_controls=ResidualControls(
                        velocity=Tolerance(
                                absolute_tolerance=1.0E-6,
                        ),
                        pressure=Tolerance(
                                absolute_tolerance=1.0E-6,
                        ),
                        turbulent_kinetic_energy=Tolerance(
                                absolute_tolerance=1.0E-6,
                        ),
                        omega_dissipation_rate=Tolerance(
                                absolute_tolerance=1.0E-6,
                        ),
                ),
                solvers=FluidSolvers(
                        velocity_solver=SmoothSolver(
                                type="SMOOTH",
                                absolute_tolerance=1.0E-6,
                                relative_tolerance=0.01,
                                smoother="GAUSSSEIDEL",
                                num_sweeps=1,
                        ),
                        pressure_solver=GAMGSolver(
                                type="GAMG",
                                absolute_tolerance=1.0E-6,
                                relative_tolerance=0.001,
                                smoother="GAUSSSEIDEL",
                                num_pre_sweeps=2,
                                num_post_sweeps=1,
                                cache_agglomeration_on=True,
                                num_cells_coarsest_level=100,
                                num_merge_levels=1,
                        ),
                        turbulent_kinetic_energy_solver=SmoothSolver(
                                type="SMOOTH",
                                absolute_tolerance=1.0E-6,
                                relative_tolerance=0.01,
                                smoother="GAUSSSEIDEL",
                                num_sweeps=1,
                        ),
                        omega_dissipation_rate_solver=SmoothSolver(
                                type="SMOOTH",
                                absolute_tolerance=1.0E-6,
                                relative_tolerance=0.01,
                                smoother="GAUSSSEIDEL",
                                num_sweeps=1,
                        ),
                ),
                schemes=Schemes(
                        time_differentiation=TimeDifferentiationSchemes(
                                for_default=SteadystateTimeDifferentiationScheme(
                                        type="STEADYSTATE",
                                ),
                        ),
                        gradient=GradientSchemes(
                                for_default=CelllimitedLeastSquaresGradientScheme(
                                        type="CELLLIMITED_LEASTSQUARES",
                                        limiter_coefficient=1.0,
                                ),
                                grad_pressure=CelllimitedLeastSquaresGradientScheme(
                                        type="CELLLIMITED_LEASTSQUARES",
                                        limiter_coefficient=1.0,
                                ),
                                grad_velocity=CelllimitedLeastSquaresGradientScheme(
                                        type="CELLLIMITED_LEASTSQUARES",
                                        limiter_coefficient=1.0,
                                ),
                        ),
                        divergence=DivergenceSchemes(
                                for_default=GaussLinearDivergenceScheme(
                                        type="GAUSS_LINEAR",
                                ),
                                div_phi_velocity=GaussLinearUpwindVGradUDivergenceScheme(
                                        type="GAUSS_LINEARUPWINDV_GRAD_U_",
                                ),
                                div_phi_turbulent_kinetic_energy=BoundedGaussUpwindDivergenceScheme(
                                        type="BOUNDED_GAUSS_UPWIND",
                                ),
                                div_nu_eff_dev_t_grad_velocity=GaussLinearDivergenceScheme(
                                        type="GAUSS_LINEAR",
                                ),
                                div_phi_omega_dissipation_rate=BoundedGaussUpwindDivergenceScheme(
                                        type="BOUNDED_GAUSS_UPWIND",
                                ),
                        ),
                        laplacian=LaplacianSchemes(
                                for_default=GaussLinearLimitedCorrectedLaplacianScheme(
                                        type="GAUSS_LINEAR_LIMITED_CORRECTED",
                                        limiter_coefficient=0.5,
                                ),
                                laplacian_nu_eff_velocity=GaussLinearLimitedCorrectedLaplacianScheme(
                                        type="GAUSS_LINEAR_LIMITED_CORRECTED",
                                        limiter_coefficient=0.5,
                                ),
                                laplacian_1_a_u_pressure=GaussLinearLimitedCorrectedLaplacianScheme(
                                        type="GAUSS_LINEAR_LIMITED_CORRECTED",
                                        limiter_coefficient=0.5,
                                ),
                                laplacian_nu_velocity=GaussLinearLimitedCorrectedLaplacianScheme(
                                        type="GAUSS_LINEAR_LIMITED_CORRECTED",
                                        limiter_coefficient=0.5,
                                ),
                        ),
                        interpolation=InterpolationSchemes(
                                for_default=LinearInterpolationScheme(
                                        type="LINEAR",
                                ),
                                interpolate_hby_a=LinearInterpolationScheme(
                                        type="LINEAR",
                                ),
                        ),
                        surface_normal_gradient=SurfaceNormalGradientSchemes(
                                for_default=LimitedSurfaceNormalGradientScheme(
                                        type="LIMITED",
                                        limiter_coefficient=0.5,
                                ),
                        ),
                ),
        ),
        simulation_control=FluidSimulationControl(
                end_time=DimensionalTime(
                        value=3000.0,
                        unit="s",
                ),
                delta_t=DimensionalTime(
                        value=1.0,
                        unit="s",
                ),
                write_control=TimeStepWriteControl(
                        type="TIME_STEP",
                        write_interval=1000,
                ),
                num_processors=-1,
                max_run_time=DimensionalTime(
                        value=10000.0,
                        unit="s",
                ),
                potential_foam_initialization=False,
                decompose_algorithm=ScotchDecomposeAlgorithm(
                        type="SCOTCH",
                ),
        ),
        result_control=FluidResultControls(
                forces_moments=[
                        ForcesMomentsResultControl(
                                type="FORCES_AND_MOMENTS",
                                name="forces_and_moments",
                                center_of_rotation=DimensionalVectorLength(
                                        value=DecimalVector(
                                                x=0.0,
                                                y=0.0,
                                                z=0.0,
                                        ),
                                        unit="m",
                                ),
                                write_control=TimeStepWriteControl(
                                        type="TIME_STEP",
                                        write_interval=5,
                                ),
                                topological_reference=TopologicalReference(
                                        entities=[
                                                "B2_TE1362",
                                                "B2_TE1510",
                                                "B2_TE1434",
                                                "B2_TE1518",
                                                "B2_TE1482",
                                                "B2_TE1550",
                                                "B2_TE1530",
                                                "B2_TE1378",
                                                "B2_TE1466",
                                                "B2_TE1594",
                                                "B2_TE1474",
                                                "B2_TE1406",
                                                "B2_TE1442",
                                                "B2_TE1558",
                                                "B2_TE1494",
                                                "B2_TE1478",
                                                "B2_TE1414",
                                                "B2_TE1562",
                                                "B2_TE1366",
                                                "B2_TE1490",
                                                "B2_TE1566",
                                                "B2_TE1586",
                                                "B2_TE1438",
                                                "B2_TE1410",
                                                "B2_TE1422",
                                                "B2_TE1402",
                                                "B2_TE1582",
                                                "B2_TE1470",
                                                "B2_TE1458",
                                                "B2_TE1454",
                                                "B2_TE1498",
                                                "B2_TE1506",
                                                "B2_TE1574",
                                                "B2_TE1526",
                                                "B2_TE1502",
                                                "B2_TE1418",
                                                "B2_TE1546",
                                                "B2_TE1570",
                                                "B2_TE1542",
                                                "B2_TE1374",
                                                "B2_TE1578",
                                                "B2_TE1486",
                                                "B2_TE1538",
                                                "B2_TE1522",
                                                "B2_TE1398",
                                                "B2_TE1534",
                                                "B2_TE1598",
                                                "B2_TE1554",
                                                "B2_TE1462",
                                                "B2_TE1386",
                                                "B2_TE1370",
                                                "B2_TE1394",
                                                "B2_TE1446",
                                                "B2_TE1382",
                                                "B2_TE1450",
                                                "B2_TE1430",
                                                "B2_TE1390",
                                                "B2_TE1514",
                                                "B2_TE1426",
                                                "B2_TE1590",
                                        ],
                                        sets=[],
                                ),
                        ),
                ],
                surface_data=[
                        AreaAverageResultControl(
                                type="AREA_AVERAGE",
                                name="area_average_inlet",
                                write_control=TimeStepWriteControl(
                                        type="TIME_STEP",
                                        write_interval=5,
                                ),
                                topological_reference=TopologicalReference(
                                        entities=[
                                                "B2_TE3336",
                                        ],
                                        sets=[],
                                ),
                        ),
                        AreaAverageResultControl(
                                type="AREA_AVERAGE",
                                name="area_average_outlet",
                                write_control=TimeStepWriteControl(
                                        type="TIME_STEP",
                                        write_interval=5,
                                ),
                                topological_reference=TopologicalReference(
                                        entities=[
                                                "B2_TE3830",
                                        ],
                                        sets=[],
                                ),
                        ),
                ],
                scalar_transport=[],
                probe_points=[],
                field_calculations=[
                        FieldCalculationsWallFluxesResultControl(
                                type="WALL_FLUXES",
                                name="Wall shear stress 1",
                                result_type=WallShearStressResultType(
                                        type="WALL_SHEAR_STRESS",
                                ),
                        ),
                        FieldCalculationsTurbulenceResultControl(
                                type="TURBULENCE",
                                name="Turbulence 2",
                                result_type=YPlusRASResultType(
                                        type="DIMENSIONLESS_WALL_DISTANCE_YPLUS",
                                ),
                        ),
                        FieldCalculationsPressureResultControl(
                                type="PRESSURE",
                                name="Pressure fields 3",
                                pressure_type=TotalPressurePressureType(
                                        type="TOTAL_PRESSURE",
                                        pressure_value=DimensionalPressure(
                                                value=0.0,
                                                unit="Pa",
                                        ),
                                ),
                                result_type=PressureValueResultType(
                                        type="PRESSURE_VALUE",
                                ),
                        ),
                ],
        ),
    )

    return model

def MeshSpec(geometry_id, edge_length=0.001):
    """
    Creates and returns meshing spec model

    Parameters
    ----------
    geometry_id: ID of the geometry element to be meshed
    edge_length: Target element size

    Returns
    -------
    model: Mesh Operation spec model
    """
    model = MeshOperation(
        name="Pump mesh",
        geometry_id=geometry_id,
        model=SimmetrixMeshingFluid(
            physics_based_meshing=True,
            automatic_layer_settings=AutomaticLayerOn(),
            advanced_simmetrix_settings=AdvancedSimmetrixFluidSettings(
                small_feature_tolerance=DimensionalLength(value=0.0002, unit="m"), gap_elements=0.05
            ),
        ),
    ),
    return model

def MultiSolve(P, project=default_project):
    """
    Simulate multiple models in parallel using SimScale API

    Parameters
    ----------
    P: list of geometry parameters vectors
    project: SimScale project object to run the simulation

    Returns
    -------
    Tuple of Lists: 
    """
    # Generate CADs
    # Uses OnShape API
    cadFiles = MultiGenCADOnShape(P, CAD_PATH, onshape_data)
    names = [XtoStr(X) for X in P]

    # Upload CADs to project
    # This has to be done in chunks of 15 due to API limits
    geometry_ids = []
    chunk_size = 15
    for file, name in zip(split(cadFiles, chunk_size), split(names, chunk_size)):
        geometry_ids.extend(project.MultiAddGeometry(file, name))

    # Setup and run simulations
    sim_spec = SimulationSpec()

    sim_ids = []
    mesh_specs = []
    for geometry_id, X, name in zip(geometry_ids, P, names):
        # Keep tracking invalid solutionss
        if geometry_id is None:
            sim_ids.append(None)
            mesh_specs.append(None)
            continue

        sim_id = project.createSimulation(sim_spec, geometry_id, name)
        mesh_spec = MeshSpec(geometry_id)
        sim_ids.append(sim_id)
        mesh_specs.append(mesh_spec)

    mesh_ids = project.MultiCreateMesh(mesh_specs, sim_ids)
    run_ids = project.MultiRunSimulation(sim_ids)

def split(a, n):
    """
    Auxiliary function to split a list into chunks of length `n`.
    The last chunk can have lenght `n` or less.

    Parameters
    ----------
    a: List to be splitted
    n: Length of chunks

    Returns
    -------
    List of chunks of length `n`
    """
    for i in range(0, len(a), n):
        yield a[i : i + n]

def GetResults(project, simulation_id, run_id):
    """
    Download and postprocess simulation results

    Parameters
    ----------
    project: SimScaleProject object
    simulation_id: ID of the relevant simulation
    run_id: ID of the simulation run to process

    Returns
    -------
    tuple: 

    
    
    """
    # Get result metadata
    results = project.simulation_run_api.get_simulation_run_results(
        project.project_id, simulation_id, run_id
    )

def main():
    for i1 in range(len(BBladeAngle)):
        for i2 in range (len(BBladeThickness)):
            for i3 in range (len(BBladeLeadingEdgeR)):
                for i4 in range (len(BBladeCount)):
                    for i5 in range (len(DDiffuserDraftAngle)):
                        for i6 in range (len(DDiffuserLength)):
                            for i7 in range (len(IInletPipeLength)):
                                for i8 in range (len(FFlowRate)):
                                    X = [BBladeAngle[i1],BBladeThickness[i2],BBladeLeadingEdgeR[i3],BBladeCount[i4],DDiffuserDraftAngle[i5],DDiffuserLength[i6],IInletPipeLength[i7],FFlowRate[i8]]
                                    P.append(X)
                                    #print(f'Running case for the configuration: {X}')

    print(P)
    MultiSolve(P)

if __name__ == "__main__":

    t0 = datetime.now()
    main()
    t1 = datetime.now()
    print(f"Total run time: {t1 - t0}")

