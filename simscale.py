# Simple wrapper for the SimScale API

import time

from simscale_sdk import *


class SimScaleProject:
    """
    Models a SimScale simulation project
    """

    def __init__(self, api_key, api_url, project_id):
        """
        This class wraps some simscale_sdk functionality with the
        aim of providing a simpler API for scripted simulation.

        The project_id can be taken from the web browser address bar
        after accessing the workbench:

        'https://www.simscale.com/workbench/?pid=project_id'

        Parameters
        ----------
        api_key: API acess key
        api_url: API endpoint URL
        project_id: ID of the project

        """
        # Initialize SimScale API client

        self.api_key = api_key
        self.api_url = api_url
        self.project_id = project_id

        # API client configuration
        self.api_key_header = "X-API-KEY"
        configuration = Configuration()
        configuration.host = self.api_url + "/v0"
        configuration.api_key = {
            self.api_key_header: self.api_key,
        }
        self.api_client = ApiClient(configuration)

        # Initialize API clients
        self.project_api = ProjectsApi(self.api_client)
        self.simulation_api = SimulationsApi(self.api_client)
        self.geometry_api = GeometriesApi(self.api_client)
        self.simulation_run_api = SimulationRunsApi(self.api_client)
        self.mesh_operations_api = MeshOperationsApi(self.api_client)
        self.storage_api = StorageApi(self.api_client)
        self.geometry_import_api = GeometryImportsApi(self.api_client)

    def addGeometry(self, path, name):
        """
        Uploads CAD file located in `path`, then mports into project with `name`

        Parameters
        ----------
        path: Local path of the CAD file to upload
        name: Name to be assigned to the imported geometry

        Returns
        -------
        geometry_id: ID of the imported geometry
        """
        # Upload CAD to storage
        try:
            with open(path, "rb") as f:
                cad = f.read()
        except OSError as e:
            raise Exception(f"File {path} does not exist.")

        storage = self.storage_api.create_storage()

        self.api_client.rest_client.PUT(
            url=storage.url,
            headers={"Content-Type": "application/octet-stream"},
            body=cad,
        )

        storage_id = storage.storage_id
        print(f"Uploaded CAD file in storage with id: {storage_id}")

        # Import CAD into project
        geometry_import = GeometryImportRequest(
            name=name,
            location=GeometryImportRequestLocation(storage_id),
            format="PARASOLID",
            input_unit="m",
            options=GeometryImportRequestOptions(
                facet_split=False,
                sewing=False,
                improve=False,
                optimize_for_lbm_solver=False,
            ),
        )

        geometry_import = self.geometry_import_api.import_geometry(
            self.project_id, geometry_import
        )
        geometry_import_id = geometry_import.geometry_import_id

        geometry_import_start = time.time()
        while geometry_import.status not in ("FINISHED", "CANCELED", "FAILED"):
            if time.time() > geometry_import_start + 900:
                raise TimeoutError()
            time.sleep(10)
            geometry_import = self.geometry_import_api.get_geometry_import(
                self.project_id, geometry_import_id
            )

        print(f"Geometry import status: {geometry_import.status}")

        geometry_id = geometry_import.geometry_id

        return geometry_id

    def createSimulation(self, spec, geometry_id, name):
        """
        Create new simulation for the given geometry, based on
        the given simulation spec.

        Parameters
        ----------
        spec: Simulation Spec model object
        geometry_id: ID of the base geometry
        name: Name to be assigned to the simulation

        Returns
        -------
        simulation_id: ID of the created simulation
        """
        simulation_spec = SimulationSpec(name=name, geometry_id=geometry_id, model=spec)
        simulation_id = self.simulation_api.create_simulation(
            self.project_id, simulation_spec
        ).simulation_id

        return simulation_id

    def createMesh(self, spec, simulation_id, max_runtime=600):
        """
        Create and compute one mesh, then link it to the referenced
        simulation.

        If the max_runtime is reached after the computation operation
        is started, a TimeOutError is raised.

        Parameters
        ----------
        spec: Mesh Operation model object
        simulation_id: ID of the base simulation
        max_runtime: Max execution time for the mesh computation

        Returns
        -------
        mesh_id: ID of the computed mesh
        """
        # Create the mesh
        mesh_operation = self.mesh_operations_api.create_mesh_operation(
            self.project_id, spec
        )

        self.mesh_operations_api.update_mesh_operation(
            self.project_id, mesh_operation.mesh_operation_id, mesh_operation
        )
        mesh_operation_id = mesh_operation.mesh_operation_id

        # Run the meshing job
        self.mesh_operations_api.start_mesh_operation(
            self.project_id, mesh_operation_id, simulation_id=simulation_id
        )

        # Wait until the meshing operation is complete
        mesh_operation = self.mesh_operations_api.get_mesh_operation(
            self.project_id, mesh_operation_id
        )

        mesh_operation_start = time.time()
        while mesh_operation.status not in ("FINISHED", "CANCELED", "FAILED"):
            if time.time() > mesh_operation_start + max_runtime:
                raise TimeoutError()
            time.sleep(30)
            mesh_operation = self.mesh_operations_api.get_mesh_operation(
                self.project_id, mesh_operation_id
            )
            print(
                f"Meshing run status: {mesh_operation.status} - {mesh_operation.progress}"
            )

        mesh_operation = self.mesh_operations_api.get_mesh_operation(
            self.project_id, mesh_operation_id
        )
        mesh_id = mesh_operation.mesh_id

        # Link the mesh to the simulation
        simulation_spec = self.simulation_api.get_simulation(
            self.project_id, simulation_id
        )
        simulation_spec.mesh_id = mesh_id
        self.simulation_api.update_simulation(
            self.project_id, simulation_id, simulation_spec
        )

        return mesh_id

    def runSimulation(self, simulation_id, name="Run", max_runtime=1800):
        """
        Create and execute one simulation run.

        If the max_runtime is reached after the computation operation
        is started, a TimeOutError is raised.

        Parameters
        ----------
        simulation_id: ID of the base simulation
        name: Name to be assigned to the simulation run
        max_runtime: Max execution time for the computation

        Returns
        -------
        run_id: ID of the created simulation run
        """
        # Create simulation run
        simulation_run = SimulationRun(name=name)
        simulation_run = self.simulation_run_api.create_simulation_run(
            self.project_id, simulation_id, simulation_run
        )
        run_id = simulation_run.run_id
        print(f"Simulation runId: {run_id}")

        # Start simulation run and wait until it's finished
        self.simulation_run_api.start_simulation_run(
            self.project_id, simulation_id, run_id
        )
        simulation_run = self.simulation_run_api.get_simulation_run(
            self.project_id, simulation_id, run_id
        )
        simulation_run_start = time.time()
        while simulation_run.status not in ("FINISHED", "CANCELED", "FAILED"):
            if time.time() > simulation_run_start + max_runtime:
                raise TimeoutError()
            time.sleep(30)
            simulation_run = self.simulation_run_api.get_simulation_run(
                self.project_id, simulation_id, run_id
            )
            print(
                f"Simulation run status: {simulation_run.status} - {simulation_run.progress}"
            )

        return run_id

    def MultiCreateMesh(self, specs, simulation_ids, max_runtime=600):
        """
        Create and compute multiple meshes in parallel,
        then link them to the referenced simulations.

        If the max_runtime is reached after the individual computation
        operations are started, a TimeOutError is raised.

        Parameters
        ----------
        specs: List of mesh operation model objects
        simulation_ids: List of IDs of the base simulations
        max_runtime: Max execution time for the mesh computations

        Returns
        -------
        mesh_ids: List of IDs of the computed meshes
        """
        # Create the mesh operations
        mesh_operation_ids = []
        for spec in specs:
            # Tracks invalid solutions
            if spec is None:
                mesh_operation_ids.append(None)
                continue

            mesh_operation = self.mesh_operations_api.create_mesh_operation(
                self.project_id, spec
            )
            self.mesh_operations_api.update_mesh_operation(
                self.project_id, mesh_operation.mesh_operation_id, mesh_operation
            )
            mesh_operation_ids.append(mesh_operation.mesh_operation_id)

        # Start all the operations at once
        print(f"Starting {len(specs)} mesh operations:")
        print(mesh_operation_ids)

        mesh_operation_start = time.time()
        for simulation_id, mesh_operation_id in zip(simulation_ids, mesh_operation_ids):
            # Do not attempt to solve invalid solutions
            if simulation_id is None:
                continue

            self.mesh_operations_api.start_mesh_operation(
                self.project_id, mesh_operation_id, simulation_id=simulation_id
            )

        # Wait for all the operations to finish
        meshes_computing = True
        while meshes_computing:
            meshes_computing = False

            for mesh_operation_id in mesh_operation_ids:
                # Do not check invalid operations
                if mesh_operation_id is None:
                    continue

                mesh_operation = self.mesh_operations_api.get_mesh_operation(
                    self.project_id, mesh_operation_id
                )

                if mesh_operation.status not in ("FINISHED", "CANCELED", "FAILED"):
                    meshes_computing = True
                    if time.time() > mesh_operation_start + max_runtime:
                        raise TimeoutError()

            time.sleep(1)

        print("All mesh operations completed!")

        # Get the IDs for the computed meshes
        mesh_ids = []
        for mesh_operation_id in mesh_operation_ids:
            # Keep track of invalid solutions
            if mesh_operation_id is None:
                mesh_ids.append(None)
                continue

            mesh_operation = self.mesh_operations_api.get_mesh_operation(
                self.project_id, mesh_operation_id
            )
            mesh_ids.append(mesh_operation.mesh_id)

        # Link the meshes to the simulations
        for simulation_id, mesh_id in zip(simulation_ids, mesh_ids):
            # Do not update invalid solutions
            if simulation_id is None:
                continue

            simulation_spec = self.simulation_api.get_simulation(
                self.project_id, simulation_id
            )
            simulation_spec.mesh_id = mesh_id
            self.simulation_api.update_simulation(
                self.project_id, simulation_id, simulation_spec
            )

        return mesh_ids

    def MultiRunSimulation(self, simulation_ids, max_runtime=1800):
        """
        Create and execute multiple simulation runs in parallel.

        If the max_runtime is reached after the computation operation
        is started, a TimeOutError is raised.

        Parameters
        ----------
        simulation_ids: IDs of the base simulations to run
        max_runtime: Max execution time for the computation

        Returns
        -------
        run_ids: List of IDs of the simulation runs
        """
        # Create the simulation runs
        simulation_run_ids = []
        for simulation_id in simulation_ids:
            # Keep track of invalid solutions
            if simulation_id is None:
                simulation_run_ids.append(None)
                continue

            simulation_run = SimulationRun(name="Run")
            simulation_run = self.simulation_run_api.create_simulation_run(
                self.project_id, simulation_id, simulation_run
            )
            run_id = simulation_run.run_id
            simulation_run_ids.append(run_id)

        # Start all the simulation runs
        print(f"Starting {len(simulation_ids)} simulation runs...")
        simulation_run_start = time.time()
        for simulation_id, run_id in zip(simulation_ids, simulation_run_ids):
            # Do not attempt to launch invalid solutions
            if run_id is None:
                continue

            self.simulation_run_api.start_simulation_run(
                self.project_id, simulation_id, run_id
            )

        # Wait for the runs to finish
        runs_computing = True
        while runs_computing:
            runs_computing = False

            for simulation_id, run_id in zip(simulation_ids, simulation_run_ids):
                # Do not check invalid solutions
                if run_id is None:
                    continue

                simulation_run = self.simulation_run_api.get_simulation_run(
                    self.project_id, simulation_id, run_id
                )

                if simulation_run.status not in ("FINISHED", "CANCELED", "FAILED"):
                    runs_computing = True
                    if time.time() > simulation_run_start + max_runtime:
                        raise TimeoutError()

            time.sleep(1)

        print("All simulation runs completed!")

        return simulation_run_ids

    def MultiAddGeometry(self, paths, names, max_runtime=600):
        """
        Uploads CAD files located in `paths`, then imports into project with `names`

        Parameters
        ----------
        paths: List of local paths of the CAD files to upload
        name: List of names to be assigned to the imported geometries
        max_runtime: Max total execution time for the geometry import operations

        Returns
        -------
        geometry_ids: List of IDs of the imported geometries
        """
        # Enforce the API limit
        if len(paths) > 15:
            raise (
                f"Maximum allowed number of simultaneous CAD uploads is 15, attempted to upload {len(paths)}"
            )

        # Upload CADs to storage and start import
        geometry_import_ids = []
        for path, name in zip(paths, names):
            try:
                with open(path, "rb") as f:
                    cad = f.read()
            except OSError as e:
                print(f"File {path} does not exist.")
                geometry_import_ids.append(None)
                continue

            storage = self.storage_api.create_storage()

            self.api_client.rest_client.PUT(
                url=storage.url,
                headers={"Content-Type": "application/octet-stream"},
                body=cad,
            )

            storage_id = storage.storage_id
            print(f"Uploaded CAD file in storage with id: {storage_id}")

            # Start CAD import
            geometry_import = GeometryImportRequest(
                name=name,
                location=GeometryImportRequestLocation(storage_id),
                format="PARASOLID",
                input_unit="m",
                options=GeometryImportRequestOptions(
                    facet_split=False,
                    sewing=False,
                    improve=True,
                    optimize_for_lbm_solver=False,
                ),
            )

            geometry_import = self.geometry_import_api.import_geometry(
                self.project_id, geometry_import
            )

            geometry_import_ids.append(geometry_import.geometry_import_id)

        # Await for the imports to finish
        geometry_import_start = time.time()
        imports_running = True
        while imports_running:
            imports_running = False

            for geometry_import_id in geometry_import_ids:
                # Do not check invalid operations
                if geometry_import_id is None:
                    continue

                geometry_import = self.geometry_import_api.get_geometry_import(
                    self.project_id, geometry_import_id
                )

                if geometry_import.status not in ("FINISHED", "CANCELED", "FAILED"):
                    imports_running = True
                    if time.time() > geometry_import_start + max_runtime:
                        raise TimeoutError()

            time.sleep(1)

        print("All import operations completed!")

        # Get the ID of the imported geometries
        geometry_ids = []
        for geometry_import_id in geometry_import_ids:
            # Keep track of invalid solutions
            if geometry_import_id is None:
                geometry_ids.append(None)
                continue

            geometry_import = self.geometry_import_api.get_geometry_import(
                self.project_id, geometry_import_id
            )

            geometry_id = geometry_import.geometry_id
            geometry_ids.append(geometry_id)

        return geometry_ids
