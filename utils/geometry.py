# Generate CAD file from parameters
# Uses SALOME and OnShape for geometry generation

import os
from subprocess import Popen, PIPE
import uuid
import onshape_client
import json


XtoStr = lambda X: "_".join([f"{xi:.1f}" for xi in X])

# Generate CAD
def GenCAD(X, CAD_PATH, SALOME_PATH):
    """
    Generate CAD file from geometry parameters (*.iges)

    Also generate file with the computed volume (*.vol)

    Files are saved in CAD_PATH

    Parameters
    ----------
    X: List with geometry parameters (len = 8)
    CAD_PATH: Path folder containing template.py and where CADs are stored
    SALOME_PATH: Salome installation folder

    Returns
    -------
    filename: Full path to the exported files without extensions
    """
    if len(X) != 12:
        raise Exception("Invalid parameters vector")

    filename = CAD_PATH + XtoStr(X)

    with open(CAD_PATH + "template.py", "r") as f:
        script = f.read()

        script = script.replace("{{CAD_PATH}}", f"{CAD_PATH}")
        script = script.replace("{{PARAMS}}", f"{X}")
        script = script.replace("{{FILENAME}}", f"{filename}")

    with open(CAD_PATH + "geo.py", "w") as f:
        f.write(script)

    os.system("python " + SALOME_PATH + "salome -w1 -t " + CAD_PATH + "geo.py")

    # KILL SALOME TO FREE MEMORY!!!
    os.system(SALOME_PATH + "kill_salome.bat")

    return filename


def MultiGenCAD(P, CAD_PATH, SALOME_PATH):
    """
    Generate multiple CAD files from geometry parameters (*.iges)

    Also generates files with the computed volume (*.vol)

    All files are saved in CAD_PATH

    Parameters
    ----------
    P: List of Lists with geometry parameters
    CAD_PATH: Path folder containing template.py and where CADs are stored
    SALOME_PATH: Salome installation folder

    Returns
    -------
    fileNameBases: Full path to the exported files (without extensions)
    """
    # Run cad generation in parallel processes
    print(f"Starting the generation of {len(P)} CAD models...")
    fileNames = []
    jobsList = []
    for X in P:
        fileName = CAD_PATH + XtoStr(X)
        fileNames.append(fileName)

        # First prepare the geometry script
        with open(CAD_PATH + "template.py", "r") as f:
            script = f.read()

            script = script.replace("{{CAD_PATH}}", f"{CAD_PATH}")
            script = script.replace("{{PARAMS}}", f"{list(X)}")
            script = script.replace("{{FILENAME}}", f"{fileName}")

        scriptFile = fileName + "g.py"
        with open(scriptFile, "w") as f:
            f.write(script)

        # Spawn the process
        print(f"Generating CAD for " + XtoStr(X))
        jobId = str(uuid.uuid4)
        job = Popen(
            ["python", SALOME_PATH + "salome", "-w1", "-t", scriptFile],
            stdout=PIPE,
            stderr=PIPE,
        )

        jobsList.append((jobId, job))

    # Wait for all the processes to finish
    for jobId, job in jobsList:
        job.wait()
        # print(f'Job stdout: {job.stdout.read(1000)}')
        # print(f'Job err: {job.stderr.read(1000)}')

    print("CAD models generated!")

    # KILL SALOME TO FREE MEMORY!!!
    os.system(SALOME_PATH + "kill_salome.bat")

    return fileNames


def GenCADOnshape(X, CAD_PATH, ONSHAPE_DATA):
    """
    Generate CAD file from geometry parameters (*.x_t)

    Also generate file with the computed volume (*.vol)

    Files are saved in CAD_PATH

    Parameters
    ----------
    X: List with geometry parameters (len = 8)
    CAD_PATH: Path folder containing template.py and where CADs are stored
    ONSHAPE_DATA: Dictionary with the access ids.

    Returns
    -------
    filename: Full path to the exported files without extensions
    volume: volume of the part
    """
    if len(X) != 12:
        raise Exception("Invalid parameters vector")

    filename = CAD_PATH + XtoStr(X) + ".x_t"

    # Onshape data
    did = ONSHAPE_DATA["did"]
    wid = ONSHAPE_DATA["wid"]
    eid = ONSHAPE_DATA["eid"]

    config = (
        f"h1={X[0]} mm;"
        f"h2={X[1]} mm;"
        f"h3={X[2]} mm;"
        f"h4={X[3]} mm;"
        f"hp1={X[4]} mm;"
        f"hp2={X[5]} mm;"
        f"v1={X[6]} mm;"
        f"v2={X[7]} mm;"
        f"v3={X[8]} mm;"
        f"v4={X[9]} mm;"
        f"r1={X[10]} mm;"
        f"r2={X[11]} mm"
    )

    os_client = onshape_client.Client()
    ps = os_client.part_studios_api.export_ps1(
        did, "w", wid, eid, configuration=config, _preload_content=False
    )

    with open(filename, "wb") as f:
        f.write(ps.data)

    mass_props = os_client.part_studios_api.get_part_studio_mass_properties(
        did, "w", wid, eid, configuration=config, _preload_content=False
    )

    volume = json.loads(mass_props.data)["bodies"]["-all-"]["volume"][0]

    return filename, volume


def MultiGenCADOnShape(P, CAD_PATH, ONSHAPE_DATA):
    """
    Generate multiple CAD files from geometry parameters (*.x_t)

    Also generates files with the computed volume (*.vol)

    All files are saved in the CAD_PATH folder

    Parameters
    ----------
    P: List of Lists with geometry parameters
    CAD_PATH: Path folder where CADs are stored

    Returns
    -------
    fileNameBases: List of full paths to the exported CAD files
    volumes: List of computed volumes
    """
    fileNames = []
    volumes = []

    # Onshape data
    did = ONSHAPE_DATA["did"]
    wid = ONSHAPE_DATA["wid"]
    eid = ONSHAPE_DATA["eid"]

    os_client = onshape_client.Client()

    # ToDo
    # Make this loop parallel for speed.
    for X in P:
        if len(X) != 12:
            raise Exception(f"Invalid parameters vector: {X}")

        filename = CAD_PATH + XtoStr(X) + ".x_t"

        config = (
            f"h1={X[0]} mm;"
            f"h2={X[1]} mm;"
            f"h3={X[2]} mm;"
            f"h4={X[3]} mm;"
            f"hp1={X[4]} mm;"
            f"hp2={X[5]} mm;"
            f"v1={X[6]} mm;"
            f"v2={X[7]} mm;"
            f"v3={X[8]} mm;"
            f"v4={X[9]} mm;"
            f"r1={X[10]} mm;"
            f"r2={X[11]} mm"
        )

        ps = os_client.part_studios_api.export_ps1(
            did, "w", wid, eid, configuration=config, _preload_content=False
        )

        with open(filename, "wb") as f:
            f.write(ps.data)

        mass_props = os_client.part_studios_api.get_part_studio_mass_properties(
            did, "w", wid, eid, configuration=config, _preload_content=False
        )

        volume = json.loads(mass_props.data)["bodies"]["-all-"]["volume"][0]

        fileNames.append(filename)
        volumes.append(volume)

    return fileNames, volumes
