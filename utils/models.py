# FEA simulation utility functions

import pandas as pd
from io import StringIO
from math import inf
import os, time

from simscale_sdk import *
from utils import geometry, simscale
from utils.geometry import XtoStr

import zipfile

CAD_PATH = "/Users/rattarzadeh/Desktop/turbine-disk-optimization/geo/"
#SALOME_PATH = r"/home/rattarzadeh/Desktop/git/turbine-disk-optimization/geo/"#"C:/Users/GuillermoGiraldo/SALOME-9.7.0/"
FOLDER = "/Users/rattarzadeh/Desktop/turbine-disk-optimization/results/"
onshape_data = {
    "did": "6bf5d81202fdad8e6af3df9a",
    "wid": "80e00851975c232753db07ab",
    "eid": "8ee9bb8e251634eaa15ed3d0",
}

# Load API access data and create project object
# with open("SIMSCALE_API_KEY", "r") as f:
#     API_KEY = f.read()
# with open("API-URL", "r") as f:
#     API_URL = f.read()
# with open("PID", "r") as f:
#     PID = f.read()

#import projectInfo

API_KEY = os.getenv('SIMSCALE_API_KEY')
API_URL = os.getenv('SIMSCALE_API_URL')
PID = 2394422977776587562
# This will be the default project to run simulations,
# if no second argument is given to SolveFEAModel()
default_project = simscale.SimScaleProject(API_KEY, API_URL, PID)


def SimulationSpec(project=default_project):
    """
    Creates and returns the simulation spec model

    Parameters
    ----------
    project: SimScaleProject object where to create the geometry objects

    Returns
    -------
    model: Static Analysis simulation spec model
    """
    p1 = Point(
        name="Point 1",
        center=DimensionalVectorLength(
            value=DecimalVector(x=0.23, y=-0.015, z=0.0), unit="m"
        ),
    )

    p2 = Point(
        name="Point 2",
        center=DimensionalVectorLength(
            value=DecimalVector(x=0.23, y=0.015, z=0.0), unit="m"
        ),
    )

    p1_id = project.simulation_api.create_geometry_primitive(
        project.project_id, p1
    ).geometry_primitive_id

    p2_id = project.simulation_api.create_geometry_primitive(
        project.project_id, p2
    ).geometry_primitive_id

    model = ThermalMechanical(
        type="THERMAL_MECHANICAL",
        time_dependency=StationaryTimeDependency(
            type="STATIONARY",
        ),
        inertia_effect="STATIC",
        non_linear_analysis=False,
        connection_groups=[
            Contact(
                type="CONTACT",
                connections=[
                    CyclicSymmetryContact(
                        type="CYCLIC_SYMMETRY",
                        name="Cyclic symmetry 1",
                        enable_heat_transfer="YES",
                        axis_origin=DimensionalVectorLength(
                            value=DecimalVector(
                                x=0,
                                y=0,
                                z=0,
                            ),
                            unit="m",
                        ),
                        axis_direction=DimensionalVectorLength(
                            value=DecimalVector(
                                x=0,
                                y=-1,
                                z=0,
                            ),
                            unit="m",
                        ),
                        sector_angle=DimensionalAngle(
                            value=1,
                            unit="°",
                        ),
                        master_topological_reference=TopologicalReference(
                            entities=[
                                "B1_TE304",
                            ],
                            sets=[],
                        ),
                        slave_topological_reference=TopologicalReference(
                            entities=[
                                "B1_TE1",
                            ],
                            sets=[],
                        ),
                    ),
                ],
            ),
        ],
        element_technology=SolidElementTechnology(
            element_technology3_d=ElementTechnology(
                mechanical_element_type="STANDARD",
                thermal_element_type="STANDARD",
                thermal_mass_order_reduction=False,
                definitions=[],
            ),
        ),
        model=SolidModel(
            magnitude=DimensionalFunctionAcceleration(
                value=ConstantFunction(
                    type="CONSTANT",
                    value=0,
                ),
                unit="m/s²",
            ),
            e=DimensionalVectorLength(
                value=DecimalVector(
                    x=0,
                    y=0,
                    z=0,
                ),
                unit="m",
            ),
        ),
        materials=[
            SolidMaterial(
                name="Steel",
                material_behavior=LinearElasticMaterialBehavior(
                    type="LINEAR_ELASTIC",
                    directional_dependency=IsotropicDirectionalDependency(
                        type="ISOTROPIC",
                        youngs_modulus=DimensionalFunctionPressure(
                            value=ConstantFunction(
                                type="CONSTANT",
                                value=217000,
                            ),
                            unit="MPa",
                        ),
                        poissons_ratio=ConstantFunction(
                            type="CONSTANT",
                            value=0.3,
                        ),
                    ),
                ),
                density=DimensionalFunctionDensity(
                    value=ConstantFunction(
                        type="CONSTANT",
                        value=8350,
                    ),
                    unit="kg/m³",
                ),
                expansion=IsotropicExpansion(
                    type="ISOTROPIC",
                    expansion_coefficient=DimensionalFunctionThermalExpansionRate(
                        value=ConstantFunction(
                            type="CONSTANT",
                            value=1.2e-5,
                        ),
                        unit="1/K",
                    ),
                    reference_temperature=DimensionalTemperature(
                        value=20,
                        unit="°C",
                    ),
                ),
                conductivity=IsotropicConductivity(
                    type="ISOTROPIC",
                    thermal_conductivity=DimensionalFunctionThermalConductivity(
                        value=ConstantFunction(
                            type="CONSTANT",
                            value=60,
                        ),
                        unit="W/(m·K)",
                    ),
                ),
                specific_heat=DimensionalFunctionSpecificHeat(
                    value=ConstantFunction(
                        type="CONSTANT",
                        value=480,
                    ),
                    unit="J/(kg·K)",
                ),
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE178",
                    ],
                    sets=[],
                ),
                built_in_material="builtInSteel",
            ),
        ],
        initial_conditions=SolidInitialConditions(),
        boundary_conditions=[
            RemoteDisplacementLoadBC(
                type="REMOTE_DISPLACEMENT_LOAD",
                name="Fix Axial",
                displacement=DimensionalPartialVectorFunctionLength(
                    value=PartialVectorFunction(
                        x=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                        y=PrescribedOptionalFunction(
                            type="PRESCRIBED",
                            value=ConstantFunction(
                                type="CONSTANT",
                                value=0,
                            ),
                        ),
                        z=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                    ),
                    unit="m",
                ),
                rotation=DimensionalPartialVectorFunctionAngle(
                    value=PartialVectorFunction(
                        x=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                        y=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                        z=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                    ),
                    unit="°",
                ),
                external_point=DimensionalVectorLength(
                    value=DecimalVector(
                        x=0,
                        y=0,
                        z=0,
                    ),
                    unit="m",
                ),
                deformation_behavior="DEFORMABLE",
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE404",
                    ],
                    sets=[],
                ),
            ),
            CentrifugalForceBC(
                type="CENTRIFUGAL_FORCE",
                name="Centrifugal force",
                rotation=AngularRotation(
                    type="ANGULAR_ROTATION",
                    rotation_center=DimensionalVectorLength(
                        value=DecimalVector(
                            x=0,
                            y=0,
                            z=0,
                        ),
                        unit="m",
                    ),
                    rotation_axis=DimensionalVectorLength(
                        value=DecimalVector(
                            x=0,
                            y=-1,
                            z=0,
                        ),
                        unit="m",
                    ),
                    angular_velocity=DimensionalFunctionRotationSpeed(
                        value=ConstantFunction(
                            type="CONSTANT",
                            value=1047,
                        ),
                        unit="rad/s",
                    ),
                ),
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE178",
                    ],
                    sets=[],
                ),
            ),
            PressureBC(
                type="PRESSURE",
                name="Blades Load",
                pressure=DimensionalFunctionPressure(
                    value=ConstantFunction(
                        type="CONSTANT",
                        value=-147,
                    ),
                    unit="MPa",
                ),
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE374",
                    ],
                    sets=[],
                ),
            ),
            FixedValueBC(
                type="FIXED_VALUE",
                name="Fix Tangent",
                displacement=DimensionalPartialVectorFunctionLength(
                    value=PartialVectorFunction(
                        x=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                        y=UnconstrainedOptionalFunction(
                            type="UNCONSTRAINED",
                        ),
                        z=PrescribedOptionalFunction(
                            type="PRESCRIBED",
                            value=ConstantFunction(
                                type="CONSTANT",
                                value=0,
                            ),
                        ),
                    ),
                    unit="m",
                ),
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE304",
                    ],
                    sets=[],
                ),
            ),
            FixedTemperatureValueBC(
                type="FIXED_TEMPERATURE_VALUE",
                name="Temp Function",
                temperature_value=DimensionalFunctionTemperature(
                    value=ExpressionFunction(
                        type="EXPRESSION",
                        expression="337.5*x -16388.9*x*y +1802.8*y+512.9",
                        available_variables=[
                            FunctionParameter(
                                parameter="temperatureValue",
                                unit="°C",
                            ),
                        ],
                    ),
                    unit="°C",
                ),
                topological_reference=TopologicalReference(
                    entities=[
                        "B1_TE178",
                    ],
                    sets=[],
                ),
            ),
        ],
        numerics=SolidNumerics(
            solver=MUMPSSolver(
                type="MUMPS",
                precision_singularity_detection=8,
                stop_if_singular=True,
                matrix_type="AUTOMATIC_DETECTION",
                memory_percentage_for_pivoting=20,
                linear_system_relative_residual=1.0e-5,
                preprocessing=True,
                renumbering_method="SCOTCH",
                postprocessing="ACTIVE",
                distributed_matrix_storage=True,
                memory_management="AUTOMATIC",
            ),
        ),
        simulation_control=SolidSimulationControl(
            processors=ComputingCore(
                num_of_processors=8,
                num_of_computing_processors=1,
            ),
            max_run_time=DimensionalTime(
                value=3600,
                unit="s",
            ),
        ),
        result_control=SolidResultControl(
            solution_fields=[
                DisplacementResultControlItem(
                    type="DISPLACEMENT",
                    name="displacement",
                    displacement_type=GlobalDisplacementType(
                        type="GLOBAL",
                    ),
                ),
                StressResultControlItem(
                    type="STRESS",
                    name="cauchy stress",
                    stress_type=GlobalCauchyStressType(
                        type="CAUCHY",
                    ),
                ),
                StressResultControlItem(
                    type="STRESS",
                    name="von Mises stress",
                    stress_type=GlobalVonMisesStressType(
                        type="VON_MISES",
                    ),
                ),
                StrainResultControlItem(
                    type="STRAIN",
                    name="total strain",
                    strain_type=GlobalTotalStrainType(
                        type="TOTAL",
                    ),
                ),
                TemperatureResultControlItem(
                    type="TEMPERATURE",
                    name="temperature",
                    temperature_type="FIELD",
                ),
            ],
            edge_calculation=[],
            area_calculation=[],
            volume_calculation=[
                MinMaxFieldsCalculationResultControlItem(
                    type="MIN_MAX_FIELDS_CALCULATION",
                    name="VMS",
                    field_selection=StressFieldSelection(
                        type="STRESS",
                        stress_type=VonMisesStressType(
                            type="VON_MISES",
                        ),
                    ),
                    topological_reference=TopologicalReference(
                        entities=[
                            "B1_TE178",
                        ],
                        sets=[],
                    ),
                ),
            ],
            point_data=[
                TemporalResponseResultControlItem(
                    type="TEMPORAL_RESPONSE",
                    name="UR1",
                    field_selection=DisplacementFieldSelection(
                        type="DISPLACEMENT",
                        component_selection="X",
                    ),
                    geometry_primitive_uuids=[
                        p1_id,
                    ],
                ),
                TemporalResponseResultControlItem(
                    type="TEMPORAL_RESPONSE",
                    name="UR2",
                    field_selection=DisplacementFieldSelection(
                        type="DISPLACEMENT",
                        component_selection="X",
                    ),
                    geometry_primitive_uuids=[
                        p2_id,
                    ],
                ),
            ],
        ),
        mesh_order="NONE",
    )

    return model


def MeshSpec(geometry_id, edge_length=0.001):
    """
    Creates and returns meshing spec model

    Parameters
    ----------
    geometry_id: ID of the geometry element to be meshed
    edge_length: Target element size

    Returns
    -------
    model: Mesh Operation spec model
    """
    model = MeshOperation(
        name="Disk mesh",
        geometry_id=geometry_id,
        model=SimmetrixMeshingSolid(
            type="SIMMETRIX_MESHING_SOLID",
            sizing=ManualMeshSizingSimmetrix(
                type="MANUAL",
                maximum_edge_length=DimensionalLength(
                    value=edge_length,
                    unit="m",
                ),
                minimum_edge_length=DimensionalLength(
                    value=edge_length,
                    unit="m",
                ),
            ),
            refinements=[],
            second_order=True,
            enable_shell_meshing=False,
            num_of_processors=-1,
            advanced_simmetrix_settings=AdvancedSimmetrixSolidSettings(
                small_feature_tolerance=DimensionalLength(
                    value=1.0e-5,
                    unit="m",
                ),
                gap_elements=0,
                global_gradation_rate=1.22,
            ),
        ),
    )

    return model


def GetResults(project, simulation_id, run_id):
    """
    Download and postprocess simulation results

    Parameters
    ----------
    project: SimScaleProject object
    simulation_id: ID of the relevant simulation
    run_id: ID of the simulation run to process

    Returns
    -------
    tuple: (maxVMS, ur1, ur2)

    maxVMS: Maximum Von Mises Stress on the whole model
    ur1: Radial deformation of end point 1
    ur2: Radial deformation of end point 2
    """
    # Get result metadata
    results = project.simulation_run_api.get_simulation_run_results(
        project.project_id, simulation_id, run_id
    )

    # Download and process results
    for res in results._embedded:

        print(f"res.type = {res.type}")

        if res.type == "SOLUTION_FIELD":
            res_data = project.api_client.rest_client.GET(
                url=res.download.url,
                headers={"X-API-KEY": API_KEY},
                _preload_content=False,
            )

            with open(FOLDER + f"res_{simulation_id}.zip", "wb") as f:
                f.write(res_data.data)

            # Uncompress results
            #with zipfile.ZipFile(FOLDER + "res.zip", "r") as z:
            #    z.extractall(FOLDER)

        if res.type == "PLOT" and res.name == "VMS":
            res_data = project.api_client.rest_client.GET(
                url=res.download.url,
                headers={project.api_key_header: project.api_key},
                _preload_content=False,
            )
            maxVMS_csv = res_data.data.decode("utf-8")
            maxVMS_df = pd.read_csv(StringIO(maxVMS_csv))

        if res.type == "PLOT" and res.name == "UR1":
            res_data = project.api_client.rest_client.GET(
                url=res.download.url,
                headers={project.api_key_header: project.api_key},
                _preload_content=False,
            )
            ur1_csv = res_data.data.decode("utf-8")
            ur1_df = pd.read_csv(StringIO(ur1_csv))

        if res.type == "PLOT" and res.name == "UR2":
            res_data = project.api_client.rest_client.GET(
                url=res.download.url,
                headers={project.api_key_header: project.api_key},
                _preload_content=False,
            )
            ur2_csv = res_data.data.decode("utf-8")
            ur2_df = pd.read_csv(StringIO(ur2_csv))

    maxVMS = maxVMS_df["VMIS (max)"].to_numpy()[0]
    ur1 = ur1_df["DX"].to_numpy()[0]
    ur2 = ur2_df["DX"].to_numpy()[0]

    return maxVMS, ur1, ur2


def SolveFEAModel(X, project=default_project):
    """
    Simulate the model using SimScale API

    Parameters
    ----------
    X: geometry parameters vector
    project: SimScale project object to run the simulation

    Returns
    -------
    Tuple: (maxVMS, ur1, ur2, vol)
    """
    # Generate CAD
    Xstr = XtoStr(X)

    cadfile, vol = geometry.GenCADOnshape(X.tolist(), CAD_PATH, onshape_data)

    # Upload CAD to project
    geometry_id = project.addGeometry(cadfile, Xstr)

    # Setup and run simulation
    sim_spec = SimulationSpec()
    sim_id = project.createSimulation(sim_spec, geometry_id, Xstr)
    # Wall thickness to control mesh element size (IN METERS!!!)
    mesh_spec = MeshSpec(geometry_id)
    mesh_id = project.createMesh(mesh_spec, sim_id)
    run_id = project.runSimulation(sim_id)

    # Extract results
    # This results are in (Pa, m)
    maxVMS, ur1, ur2 = GetResults(project, sim_id, run_id)

    # Units conversion
    maxVMS = maxVMS / 1e6  # Pa -> MPa
    ur1 = ur1 * 1e3  # m -> mm
    ur2 = ur2 * 1e3  # m -> mm

    # Units of m^3 per 1°
    vol = float(vol)

    return maxVMS, ur1, ur2, vol


def MultiSolve(P, project=default_project):
    """
    Simulate multiple models in parallel using SimScale API

    Parameters
    ----------
    P: list of geometry parameters vectors
    project: SimScale project object to run the simulation

    Returns
    -------
    Tuple of Lists: ([maxVMS, MPa], [ur1, mm], [ur2, mm], [volumes, m3/°])
    """
    # Generate CADs
    # Uses OnShape API
    cadFiles, volumes = geometry.MultiGenCADOnShape(P, CAD_PATH, onshape_data)
    names = [XtoStr(X) for X in P]

    # Upload CADs to project
    # This has to be done in chunks of 15 due to API limits
    geometry_ids = []
    chunk_size = 15
    for file, name in zip(split(cadFiles, chunk_size), split(names, chunk_size)):
        geometry_ids.extend(project.MultiAddGeometry(file, name))

    # Setup and run simulations
    sim_spec = SimulationSpec()

    sim_ids = []
    mesh_specs = []
    for geometry_id, X, name in zip(geometry_ids, P, names):
        # Keep tracking invalid solutionss
        if geometry_id is None:
            sim_ids.append(None)
            mesh_specs.append(None)
            continue

        sim_id = project.createSimulation(sim_spec, geometry_id, name)
        mesh_spec = MeshSpec(geometry_id)
        sim_ids.append(sim_id)
        mesh_specs.append(mesh_spec)

    mesh_ids = project.MultiCreateMesh(mesh_specs, sim_ids)
    run_ids = project.MultiRunSimulation(sim_ids)

    # Extract Simulation Results
    maxVMS = []
    ur1 = []
    ur2 = []
    for sim_id, run_id in zip(sim_ids, run_ids):
        # Track invalid solutions
        if run_id is None:
            maxVMS.append(inf)
            ur1.append(inf)
            ur2.append(inf)
            continue

        vms, u1, u2 = GetResults(project, sim_id, run_id)
        maxVMS.append(vms / 1e6)  # Convert to MPa
        ur1.append(u1 * 1e3)  # Convert to mm
        ur2.append(u2 * 1e3)  # Convert to mm

    return maxVMS, ur1, ur2, volumes


def split(a, n):
    """
    Auxiliary function to split a list into chunks of length `n`.
    The last chunk can have lenght `n` or less.

    Parameters
    ----------
    a: List to be splitted
    n: Length of chunks

    Returns
    -------
    List of chunks of length `n`
    """
    for i in range(0, len(a), n):
        yield a[i : i + n]